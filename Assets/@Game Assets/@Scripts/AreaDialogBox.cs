﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AreaDialogBox : MonoBehaviour {

	public Image[] portraits;

	public void ChangePortrait(int index)
	{
		for (int i = 0; i < portraits.Length; i++)
		{
			portraits[i].gameObject.SetActive(false);
		}

		portraits [index].gameObject.SetActive (true);
	}
}
