﻿using UnityEngine;
using System.Collections;

public class Avatar : MonoBehaviour 
{
	public string title = "Tom Hanks";
	public float maxHealth = 100f;
	public float maxStamina = 100f;
	public RoleNames role;
	public CharAttribute attributes;

	public float health;
	public float stamina;
	public bool isSick;
	public bool isMissing;
	
	public bool isAlive {
	  get { return health > 0; }
	}
	
	// events
	public delegate void StatsChangedHandler();
	public event StatsChangedHandler onStatsChangedE;
	public void OnStatsChanged() { if (onStatsChangedE != null)	onStatsChangedE ();}

	void Start()
	{
		InitStatus ();
	}

	void InitStatus ()
	{
		health = maxHealth;
		stamina = maxStamina;

		OnStatsChanged ();
	}

	public void AddHealth(float amount)
	{
		if(!isAlive) return;
		
		health = Mathf.Clamp (health + amount, 0, maxHealth);
		if(health <= 0) {
		  Debug.Log(title + " died!");
		  gameObject.SetActive(false);
		}
		OnStatsChanged ();
	}

	public void AddStamina(float amount)
	{
		stamina = Mathf.Clamp (stamina + amount, 0, maxStamina);
		OnStatsChanged ();
	}

}
