﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class AvatarDisplay : MonoBehaviour
{
	public Avatar dataItem;

	public Button avatarButton;
	public Button avatarCureButton;
	public Text nameText;
	public Text healthText;
	public Text staminaText;
	public Text foodCostText;
	public Text dialogText;
	public GameObject sickAnim;

	public Animator portraitAnim;
	public Animator dialogAnim;
	public Animator statusAnim;

	public bool isStatusPopup;

	void Awake()
	{
		dataItem = this.GetComponent<Avatar> ();
	}

	void OnEnable()
	{
		dataItem.onStatsChangedE += OnStatsChanged;

		EventsManager.onAvatarSelectE += OnAvatarSelect;
		EventsManager.onAvatarDeselectE += OnAvatarDeselect;
		EventsManager.onAvatarCureSelectE += OnAvatarCureSelect;
		EventsManager.onAvatarCureDeselectE += OnAvatarCureDeselect;
		EventsManager.onStateEnterE += OnStateEnter;
		EventsManager.onStateExitE += OnStateExit;
	}

	void OnDisable()
	{
		dataItem.onStatsChangedE -= OnStatsChanged;

		EventsManager.onAvatarSelectE -= OnAvatarSelect;
		EventsManager.onAvatarDeselectE -= OnAvatarDeselect;
		EventsManager.onAvatarCureSelectE -= OnAvatarCureSelect;
		EventsManager.onAvatarCureDeselectE -= OnAvatarCureDeselect;
		EventsManager.onStateExitE -= OnStateEnter;
		EventsManager.onStateExitE -= OnStateExit;
	}

	void OnStatsChanged ()
	{
		nameText.text = dataItem.title;
		healthText.text = dataItem.health.ToString() + "/" + dataItem.maxHealth.ToString ();
		staminaText.text = dataItem.stamina.ToString() + "/" + dataItem.maxStamina.ToString ();
		foodCostText.text = dataItem.attributes.foodCost.ToString () + "/day";
	}

	void OnAvatarSelect (Avatar avatar)
	{
		if (avatar != this.dataItem)
			SetActiveAnimTalking(false);
	}
	
	void OnAvatarDeselect ()
	{
		SetActiveAnimTalking (false);
	}

	void OnAvatarCureSelect (Avatar avatar)
	{
		if (avatar != this.dataItem)
			SetActiveAnimTalking(false);
	}
	
	void OnAvatarCureDeselect ()
	{
		SetActiveAnimTalking (false);
	}

	void OnStateEnter (GameStateNames state)
	{
		switch (state)
		{
		case GameStateNames.DayPhase:
			avatarButton.gameObject.SetActive(true);
			avatarCureButton.gameObject.SetActive(false);

			if (dataItem.isSick)
				sickAnim.gameObject.SetActive(true);
			else
				sickAnim.gameObject.SetActive(false);
			break;


		case GameStateNames.CurePhase:
			avatarButton.gameObject.SetActive (false);
			avatarCureButton.gameObject.SetActive (true);
			break;
		}
	}
	
	void OnStateExit (GameStateNames state)
	{
		switch (state)
		{
		case GameStateNames.AvatarSelect:
			SetActiveAnimTalking(false);
			break;

		case GameStateNames.CureAvatarSelect:
			SetActiveAnimTalking(false);
			break;
		}

	}

	public void OnAvatarClicked()
	{
		if (!isStatusPopup)
		{
			SetActiveAnimTalking(true);
			EventsManager.OnAvatarSelect(this.dataItem);
		}
		else
		{
			SetActiveAnimTalking(false);
			EventsManager.OnAvatarDeselect();
		}
	}

	public void OnAvatarCureClicked()
	{
		if (!isStatusPopup)
		{
			SetActiveStatusInfo(true);
			EventsManager.OnAvatarCureSelect(this.dataItem);
		}
		else
		{
			SetActiveStatusInfo(false);
			EventsManager.OnAvatarCureDeselect();
		}
	}

	public void SetActiveAnimTalking(bool isActive, string dialog = "Anyeong Haseo")
	{
		dialogAnim.SetBool("FadeIn", isActive);

		if (isActive)
		{
			string[] chosenString = CharacterRandomComments.Dialogs[(int)dataItem.role];
			dialogText.text = chosenString [Random.Range (0, chosenString.Length)];

		}
		SetActiveStatusInfo (isActive);
	}

	public void SetActiveStatusInfo(bool isActive)
	{
		portraitAnim.SetBool("talk", isActive);
		statusAnim.SetBool("FadeIn", isActive);
		isStatusPopup = isActive;
	}

	public IEnumerator PopupDialog(string dialog, float duration = 3f)
	{
		dialogAnim.SetBool ("FadeIn", true);
		dialogText.text = dialog;

		yield return new WaitForSeconds (duration);

		if (!isStatusPopup)
			dialogAnim.SetBool ("FadeIn", false);
	}


}