using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeginDayResult 
{
	public int missingId = -1;
	public List<int> sickId = new List<int>();

	public BeginDayResult()
	{
		this.missingId = -1;
		this.sickId = new List<int>();
	}
}