using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharAttribute
{
	public float food;
	public float wood;
	public int energy;
	public float survive;
	public float resistance;
	public float explorability;
	public int foodCost;
}