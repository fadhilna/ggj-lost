using System.Collections.Generic;

public class CharacterDieComments
{
	public static string[] Dialog = new string[]
	{
		"No one can cure us now",
		"He's like a father to us all",
		"What a shame....",
		"He's too young to die."
	};
}