using System.Collections.Generic;

public class CharacterMapInfoComments
{
	public static List<string[]> Dialogs = new List<string[]>
	{
		new string[]
		{
		"Easy and simple, where would you rather be if you need fish?",
		"This place is the safest spot to find food.",
		"We need food, but each day we keep getting less of it.",
		},
		new string[]
		{
		"Maybe we can find alternatives for food here",
		"Many food items damaged by the weather.",
		"The food here is getting scarce.",
		},
		new string[]
		{
		"I see I can gather quite some branch here",
		"It's getting hard to find branches here.",
		"Its impossible to find any wood inhere.",
		},
		new string[]
		{
		"Fruits are perfect choice for diet balance but gotta look out for snakes",
		"I saw wild animals foot prints here, they also looking for the fruits.",
		"Ugh, you only find rotten fruits everywhere.",
		},
		new string[]
		{
		"In case we need even more wood, but I can hear beasts' roar from there",
		"Chopping wood is tiring, but we must do it.",
		"Get more woods or die from the cold.",
		},
		new string[]
		{
		"There're a lot of deer here, but if there're herbivores, there're carnivores",
		"If we don't eat meat, we all will perish.",
		"I Hope it's not the last of the deer...",
		},
		new string[]
		{
		"The lake is full of fish, and keep in mind some wild boars are seen on that area",
		"I barely see any fish now in the lake.",
		"The lake now full of trash and so few fish left.",
		},
		new string[]
		{
		"Even more huge jungle deep in the island but who know what's waiting for me inside",
		"We need to go depper into the jungle, and risk being lost.",
		"Our last choice to keep our fire going...",
		}
	};
}