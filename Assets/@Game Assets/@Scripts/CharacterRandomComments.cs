using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterRandomComments
{
	public static List<string[]> Dialogs = new List<string[]>
	{
		new string[]
		{
			"Aah I'm hungry, why won't somebody get me more food.",
			"Why would I have to stuck in here with you guys...",
			"Ha, I can't imagine how you'd survive without me here.",
			"So lucky that my medical box magically kept in tact with me.",
			"I am practically your God of death here.",
			"Do you guys ever try harder to get more supplies here?",
			"Diego's calmness give me a creep.",
			"(munch) (munch) (munch)",
			"I know everyone must be hate me to the core, but without me illness will kill them all.",
			"People like wasting money and time on useless things. Mobile game for example.",
			"I used to be called the wealthiest man in my city.",
			"We can save more food supplies if there are fewer of us...",
			"What kind of name is Woody?",
			"I swear Rala have a crush on me. I mean, who can resist my charm?",
			"I must save the medicine enough for myself. The others can just die whenever.",
			"This cursed island's weather is unpredictable! I can't sleep!",
			"What do we do now?",
			"I want pepperoni pizza. And I want it NOW!",
			"I hate bananas.",
			"Don't be a coward! This is just a sprinkle.",
			"Damn slow you are! Bunch of foolish people.",
			"We don't have that many supplies for 2-3 days ahead.",
			"Everyone hate me? Good. There's nothing they can do about it anyway."
		},
		new string[]
		{
			"Sometime I wish I would do any other thing than gathering wood.",
			"Lucky I have my infinite cigar with me here",
			"My meat is hard and sour... Just saying...",
			"Everyone warm enough? Good.",
			"My wife, she's part Indonesian and part Japanese.",
			"I hope that fatty got toothache, I'll see how his great medical skill help him",
			"Scavenge is the only way to ensure we can survive longer.",
			"This axe is so heavy, but wood is as important as food here",
			"Now let's see... What can we lit the fire with?",
			"That fatty got lucky he is a doctor. I can't wait to throw him out to the beasts if he's no use to us.",
			"Diego is such a smart kid. He reminds me of my son.",
			"Diego... Where have I heard that name before?",
			"I think I can manage two days without sleep.",
			"It sucks when my cigarette went out. Luckily I have my lighter with me.",
			"Have you guys ever been to Indonesia?",
			"If we can't conserve our woods, we will surely dead because of hypotermia.",
			"Bad weather is bad news. We'll risk our lives if we go out on that weather.",
			"What do we do now?",
			"Which is better? Stranded or zombie apocalypse?",
			"I played timberman on Android once. It was great.",
			"We don't know how long will it last!",
			"Let us discuss it later."
		},
		new string[]
		{
			"Hey! Who's farting?",
			"I miss my old archeology-hunting day",
			"Have you guys ever played this game called 'Romb Taider'?",
			"Can you gues my age?",
			"I'm pretty sure we passed that wild animal several times near the lake.",
			"It's been a long time since I take a bath, but that tentacle keep us away from the water source.",
			"Tentacle-human relationship banned a long time ago. Don't even start.",
			"I wonder what my little brother doing. Maybe he's participating on Global Game Jam right now.",
			"Bow hunting is my specialty. You can count on my eagle eyes.",
			"I'm thinking of starting a farm. We don't even know when the rescue comes.",
			"My full name? It's Rala Craft. Why do you ask?",
			"Do you guys ever thought that we could be in a videogame?",
			"Eugene's stare is getting on my nerves. I would rather be with tentacles than with him.",
			"I like video games. A lot. I wish I can bring my PSP here.",
			"We consists of 4 people. 4 minus 1 is 3. HALF LIFE 3 CONFIRMED.",
			"I can take any weather. As long as I have my raincoat on.",
			"What do we do now?",
			"Don't expect fan service from me. Ask the artist.",
			"I heard voices from the volcano. Like someone told me to throw my wedding ring there.",
			"This island... looks like Bali."
		},
		new string[]
		{
			"Do you know that there's this drug that can shrink your body into a child again?",
			"I wish I just stranded on some treasure island.",
			"Maybe it's my imagination, but I think I see some tentacles thing on the beach.",
			"Those places beneath the forest... I wonder if we can explore that far.",
			"If you encounter the beast on your way, just shout: YOU SHALL NOT PASS.",
			"I really hate that fat guy. I'm sure he's eating way more than us.",
			"Sometimes it's wise not to exploit a single person no matter how good they are",
			"Woody is so strong! He can lift many woods at once!",
			"If my best friend is here right now, she can lead us through the wood with her magical map.",
			"I told Eugene to not swiping once, it didn't end well.",
			"I like to travel to the forest. Monkeys there are so cute and friendly.",
			"I love to craft things, but I don't know how.",
			"Can we survive?",
			"What do we do now?",
			"Sometimes I wonder why we don't have a face.",
			"I was an adventurer like you, until I took an arrow to the knee.",
			"Turns out Rala is a gamer. Sweet!",
			"Trust everyone, but don't put your life on it.",
			"Do you know that TV shows on OHB: The Working Dead?",
			"I saw that fat guy stole Woody food earlier, but I'm too scared to talk.",
			"If we aren't careful enough, the plan will backfires to us..."
		}
	};
}