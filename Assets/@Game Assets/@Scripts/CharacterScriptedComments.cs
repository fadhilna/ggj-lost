using UnityEngine;
using System.Collections.Generic;

using System.Collections.Generic;

public class CharacterScriptedComments
{
	public List<DialogItem[]> Dialogs = new List<DialogItem[]>
	{
		new DialogItem[]
		{
			new DialogItem(2, "Oh myGod, what happened to your shoulder?"),
			new DialogItem(1, "This axe got me real good. It's a deep wound and I lost too much blood."),
			new DialogItem(0, "That wound is just a scratch, you pussy."),
			new DialogItem(0, "I can heal those scratch in no time.. If you guys bring me food everyday,"),
			new DialogItem(1, "Rather than that, I say we need to burn something to heat us up. This place is dead cold."),
			new DialogItem(1, "Rather than that, I say we need to burn something to heat us up. This place is dead cold."),
			new DialogItem(1, "I can help collecting woods but this wound need to be patched up."),
			new DialogItem(3, "We need to converse the supplies we had until rescue comes."),
			new DialogItem(3, "So scheduled plan is a must, like MMO's")
		},
		new DialogItem[]
		{
			new DialogItem(1, "...."),
			new DialogItem(0, "...."),
			new DialogItem(2, "...."),
			new DialogItem(3, "...."),
			new DialogItem(1, "This storm is pretty thick. We'll risk our safety if we push it through."),
			new DialogItem(0, "We will starve to death if we don't search for food now, you bunch of foolish people!"),
			new DialogItem(2, "We still have many supplies left. I suggest we wait until the storm calms down."),
			new DialogItem(3, "It's raining pretty hard. I don't think we can get through it.")
		},
		new DialogItem[]
		{
			new DialogItem(2, "...."),
			new DialogItem(0, "...."),
			new DialogItem(1, "...."),
			new DialogItem(3, "...."),
			new DialogItem(2, "I can't take it anymore! This island is trying to kill me!"),
			new DialogItem(0, "Where are the rescue team? I'm starting to lose hope..."),
			new DialogItem(1, "I want to get out from here... Please."),
			new DialogItem(3, "Mommy... I want to go home.")				
		}
	};
		
}