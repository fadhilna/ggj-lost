using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterStartDayComments
{
	public static List<string[]> Dialogs = new List<string[]>
	{
		new string[]
		{
			"Work harder people! or else...",
			"I hope one of you get lost in the fog.",
			"I Hate rains!",
			"Dark days are coming."
		},
		new string[]
		{
			"Perfect day to scavenge.",
			"It's foggy...",
			"The rain makes our survival harder.",
			"We just need to weather the storm.",
		},
		new string[]
		{
			"What a fine day.",
			"Watch out and don't bump the doctor.",
			"Singing in the rains....",
			"Storm is coming!"
		},
		new string[]
		{
			"Let's go out and play!",
			"Fog makes me hard to see.",
			"Dangerous to scavenge in the rain.",
			"The storm will pass, hopefully."
		}
	};
}