﻿using UnityEngine;
using System.Collections;

public class DangerEvent:MonoBehaviour {
	public DangerType type;
	public float chance;
	public int minDamage;
	public int maxDamage;
	public float minResourceLoss; // 1 is 100%
	public float maxResourceLoss;
	public string desc;
}
