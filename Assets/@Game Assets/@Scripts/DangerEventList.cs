﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum DangerType {
  None,
  WildAnimal,
  PoisonSnake,
  PoisonMushroom,
  Tentacle,
  Missing
}

public class DangerEventList : MonoBehaviour {
	
	public DangerEvent[] dangerEvents;
	
	float totalChance = 0;
	List<float> chanceSegments;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void Awake() {
		float currentChangeSegment = 0;
		chanceSegments = new List<float>();
		for(int i=0;i<dangerEvents.Length;i++) {
			currentChangeSegment += dangerEvents[i].chance;
			totalChance += dangerEvents[i].chance;
			chanceSegments.Add(currentChangeSegment);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public DangerEvent PickDanger() {
		float pick = Random.Range(0, totalChance);
		for(int i=0;i<chanceSegments.Count;i++) {
			if(pick < chanceSegments[i]) {
				Debug.Log("picked danger: "+dangerEvents[i].desc);
				return dangerEvents[i];
			}
		}
		return null;
	}
}
