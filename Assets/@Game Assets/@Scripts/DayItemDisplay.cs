﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DayItemDisplay : MonoBehaviour 
{
	public int dayNumber;
	public Image[] dayImages;

	public void ChangeDay(int value)
	{
		for (int i = 0; i < 5; i++)
		{
			dayImages[i].gameObject.SetActive(false);
		}

		if (value >= 0)
		{
			dayImages[value].gameObject.SetActive(true);
		}
	}
}
