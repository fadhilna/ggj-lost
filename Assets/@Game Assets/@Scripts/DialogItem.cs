using System.Collections.Generic;

[System.Serializable]
public class DialogItem
{
	public int id;
	public string dialog;

	public DialogItem(int id, string dialog)
	{
		this.id = id;
		this.dialog = dialog;
	}
}