using UnityEngine;
using System.Collections;

public class EventsManager
{
	public delegate void StateEnterHandler(GameStateNames state);
	public delegate void StateExitHandler(GameStateNames state);
	public delegate void AvatarSelectHandler(Avatar avatar);
	public delegate void AvatarDeselectHandler();
	public delegate void AvatarCureSelectHandler(Avatar avatar);
	public delegate void AvatarCureDeselectHandler();
	public delegate void DoNothingHandler();
	public delegate void CureCommandHandler();
	public delegate void CureStartHandler(Avatar targetAvatar);
	public delegate void CureCancelHandler();
	public delegate void CureFinishHandler(Avatar targetAvatar);
	public delegate void CureNotEnoughEnergyHandler();
	public delegate void ScavengeCommandHandler(Avatar avatar);
	public delegate void HuntingPlaceSelect(HuntingPlace huntPlace);
	public delegate void HuntingPlaceCancel();
	public delegate void ScavengeStartHandler(HuntingPlace huntPlace);
	public delegate void ScavengeNotEnoughEnergyHandler();
	public delegate void ScavengeFinishHandler (ScavangeResult result);
	public delegate void ResultCompleteHandler();
	public delegate void DayPhaseChangedHandler ();
	public delegate void DayChangedHandler();
	public delegate void DayChangedFinishHandler ();
	public delegate void DayBeginFinishHandler (BeginDayResult result);

	public static event StateEnterHandler onStateEnterE;
	public static event StateExitHandler onStateExitE;
	public static event AvatarSelectHandler onAvatarSelectE;
	public static event AvatarDeselectHandler onAvatarDeselectE;
	public static event AvatarCureSelectHandler onAvatarCureSelectE;
	public static event AvatarCureDeselectHandler onAvatarCureDeselectE;
	public static event DoNothingHandler onDoNothingE;
	public static event CureCommandHandler onCureCommandE;
	public static event CureCancelHandler onCureCancelE;
	public static event CureStartHandler onCureStartE;
	public static event CureFinishHandler onCureFinishE;
	public static event CureNotEnoughEnergyHandler onCureNotEnoughEnergyE;
	public static event ScavengeCommandHandler onScavengeCommandE;
	public static event HuntingPlaceSelect onHuntingPlaceSelectE;
	public static event HuntingPlaceCancel onHuntingPlaceCancelE;
	public static event ScavengeStartHandler onScavengeStartE;
	public static event ScavengeNotEnoughEnergyHandler onScavengeNotEnoughEnergyE;
	public static event ScavengeFinishHandler onScavengeFinishedE;
	public static event ResultCompleteHandler onResultCompleteE;
	public static event DayPhaseChangedHandler onDayPhaseChangedE;
	public static event DayChangedHandler onDayChangedE;
	public static event DayChangedFinishHandler onDayChangeFinishE;
	public static event DayBeginFinishHandler onDayBeginFinishE;

	public static void OnStateEnter(GameStateNames state){ if (onStateEnterE != null) onStateEnterE (state); }
	public static void OnStateExit(GameStateNames state) { if (onStateExitE != null) onStateExitE (state); }
	public static void OnAvatarSelect(Avatar avatar) { if (onAvatarSelectE != null) onAvatarSelectE(avatar); }
	public static void OnAvatarDeselect() { if (onAvatarDeselectE != null) onAvatarDeselectE(); }
	public static void OnAvatarCureSelect(Avatar avatar) { if (onAvatarCureSelectE != null) onAvatarCureSelectE(avatar);}
	public static void OnAvatarCureDeselect() { if (onAvatarCureDeselectE != null) onAvatarCureDeselectE(); }
	public static void OnDoNothing() { if (onDoNothingE != null) onDoNothingE(); }
	public static void OnCureCommand() { if (onCureCommandE != null) onCureCommandE(); }
	public static void OnCureCancel() { if (onCureCancelE != null) onCureCancelE(); }
	public static void OnCureStart(Avatar targetAvatar) { if (onCureStartE != null) onCureStartE(targetAvatar); }
	public static void OnCureFinish(Avatar targetAvatar) { if (onCureFinishE != null) onCureFinishE(targetAvatar); }
	public static void OnCureNotEnoughEnergy() { if (onCureNotEnoughEnergyE != null) onCureNotEnoughEnergyE(); }
	public static void OnScavengeCommand(Avatar avatar) { if (onScavengeCommandE != null) onScavengeCommandE(avatar); }
	public static void OnHuntingPlaceSelect(HuntingPlace huntPlace) { if (onHuntingPlaceSelectE != null) onHuntingPlaceSelectE(huntPlace); }
	public static void OnHuntingPlaceCancel() { if (onHuntingPlaceCancelE != null) onHuntingPlaceCancelE(); }
	public static void OnScavengeStart(HuntingPlace huntPlace) { if (onScavengeStartE != null) onScavengeStartE(huntPlace); }
	public static void OnScavengeNotEnoughEnergy() { if (onScavengeNotEnoughEnergyE != null) onScavengeNotEnoughEnergyE(); }
	public static void OnScavengeFinish(ScavangeResult result) { if (onScavengeFinishedE != null) onScavengeFinishedE(result); }
	public static void OnResultComplete() { if(onResultCompleteE != null) onResultCompleteE(); }
	public static void OnDayPhaseChanged() { if (onDayPhaseChangedE != null) onDayPhaseChangedE(); }
	public static void OnDayChanged() { if (onDayChangedE != null) onDayChangedE(); }
	public static void OnDayChangedFinish() { if (onDayChangeFinishE != null) onDayChangeFinishE(); }
	public static void OnDayBeginFinish(BeginDayResult result) { if (onDayBeginFinishE != null) onDayBeginFinishE(result); }
}