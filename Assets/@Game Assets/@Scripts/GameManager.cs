﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : FSMSystem
{
		public static GameManager Instance;
		public int day;
		public int phaseDay;

		// ============ global stats
		public ResourceItem food;
		public ResourceItem wood;
		public WeatherSystem weather;
		public WeatherCondition currentWeather;
		public DangerEventList dangerList;

		// ============ game states
		public GameState_None s_None;
		public GameState_BeginDay s_BeginDay;
		public GameState_DayPhase s_DayPhase;
		public GameState_AvatarSelect s_AvatarSelect;
		public GameState_CurePhase s_CurePhase;
		public GameState_CureAvatarSelect s_CureAvatarSelect;
		public GameState_CureResult s_CureResult;
		public GameState_AreaSelect s_AreaSelect;
		public GameState_HuntingResult s_HuntingResult;
		public GameState_EndPhase s_EndPhase;
		public GameState_EndDay s_EndDay;
		public GameState_GameOver s_GameOver;
		public GameState_Ending s_Ending;
		public GameState_ScriptedDialog s_ScriptedDialog;

		// ============= avatars
		public Avatar activeAvatar;
		public Avatar[] avatars;

		// ============ hunting place
		public HuntingPlace activeHuntingPlace;
		public List<HuntingPlace> huntingPlaces;
	
		//============== helpers
		public float TotalFoodCost {
				get {
						float tfc = 0;
						foreach (Avatar avt in avatars) {
								if (!avt.isMissing || !avt.isAlive)
										tfc += avt.attributes.foodCost;
						}
						//Debug.Log("total food cost is "+tfc);
						return tfc;
				}
		}

		public float TotalWoodCost {
				get {
						return Mathf.Round (GameParameters.Instance.basicWoodExcess + (10 * currentWeather.obstacle));
				}
		}

		public bool APersonIsMissing {
				get {
						foreach (Avatar avt in avatars)
								if (avt.isMissing)
										return true;
						return false;
				}
		}

		public ScavangeResult scavangeResult;
		public float totalEnergyCost;
		public DangerEvent missingDangerInfo;

		public bool isGameOver {
				get {
						foreach (Avatar avt in avatars) {
								if (avt.isAlive)
										return false;
						}
						return true;
				}
		}

		public bool isGameWin = false;

		public Avatar MedicAvatar {
				get {
						foreach (Avatar avt in avatars) {
								if (avt.role == RoleNames.MEDIC)
										return avt;
						}
						return null;
				}
		}

		public BeginDayResult beginDayResult;

		// ============= events
		public delegate void StatsChangedHandler ();

		public event StatsChangedHandler onStatsChangedE;

		public void OnStatsChanged ()
		{
				if (onStatsChangedE != null)
						onStatsChangedE ();
		}

		void Awake ()
		{
				Instance = this;
		}

		void Start ()
		{
				RegisterStates ();
		}

		void OnEnable ()
		{
				EventsManager.onAvatarSelectE += OnAvatarSelect;
				EventsManager.onAvatarCureSelectE += OnAvatarCureSelect;
				EventsManager.onCureStartE += OnCureStart;
				EventsManager.onHuntingPlaceSelectE += OnHuntingPlaceSelect;
				EventsManager.onScavengeCommandE += OnScavengeCommand;
				EventsManager.onScavengeStartE += OnScavengeStart;
				EventsManager.onDoNothingE += OnDoNothing;

				EventsManager.onStateEnterE += OnStateEnter;
				EventsManager.onStateExitE += OnStateExit;
		}

		void OnDisable ()
		{
				EventsManager.onAvatarSelectE -= OnAvatarSelect;
				EventsManager.onAvatarCureSelectE -= OnAvatarCureSelect;
				EventsManager.onCureStartE -= OnCureStart;
				EventsManager.onHuntingPlaceSelectE -= OnHuntingPlaceSelect;
				EventsManager.onScavengeCommandE -= OnScavengeCommand;
				EventsManager.onScavengeStartE -= OnScavengeStart;
				EventsManager.onDoNothingE -= OnDoNothing;

				EventsManager.onStateExitE -= OnStateEnter;
				EventsManager.onStateExitE -= OnStateExit;
		}
	
		void OnStateEnter (GameStateNames state)
		{
				switch (state) {
				case GameStateNames.BeginDay:
						BeginDayRoutine ();
						break;

				case GameStateNames.EndPhase:
						AddDayPhase ();
						break;
			
				case GameStateNames.EndDay:
						EndDayRoutine ();
						break;
				}
		}
	
		void OnStateExit (GameStateNames state)
		{
		
		}
	
		void BeginDayRoutine ()
		{
				Debug.Log ("New day arrives");
				LogText.Instance.Log ("======== Begin new day routine ===========");

				beginDayResult = new BeginDayResult ();
				beginDayResult.sickId = new List<int> ();

				foreach (Avatar avt in avatars) {
						if (avt.isAlive) {
								if (avt.isMissing) {
										Debug.Log (avt.title + " returned to camp");
										avt.isMissing = false;
										avt.gameObject.SetActive (true);
								} else
										beginDayResult.missingId = MissingChance (avt);
			  
				int sickId = SickChance(avt);
								if (sickId >= 0)
					beginDayResult.sickId.Add(sickId);
		    
								avt.AddStamina (GameParameters.ENERGY_REGEN_PER_DAY);
						}
				}
		
		
				currentWeather = weather.PickWeatherSet (day).PickWeather ();
		
				if (day >= 21 && !isGameOver)
						isGameWin = true;

				this.OnStatsChanged ();
		EventsManager.OnDayBeginFinish(beginDayResult);

				int randId = Random.Range (0, avatars.Length);
				StartCoroutine (avatars [randId].GetComponent<AvatarDisplay> ().PopupDialog (CharacterStartDayComments.Dialogs [randId] [(int)currentWeather.type]));
		}
	
		void EndDayRoutine ()
		{
				// reduce the value of food and wood per day
				if (TotalFoodCost > food.value) {
						float dmgValue = (TotalFoodCost - food.value) * GameParameters.FOOD_SHORTAGE_DMG_MULTI;
						foreach (Avatar avt in avatars) {
								avt.AddHealth (-dmgValue);
						}
				}
				food.AddValue (-TotalFoodCost);
		
				if (TotalWoodCost > wood.value) {
						float dmgValue = (TotalWoodCost - wood.value) * GameParameters.WOOD_SHORTAGE_DMG_MULTI;
						foreach (Avatar avt in avatars) {
								avt.AddHealth (-dmgValue);
						}
				}
				wood.AddValue (-TotalWoodCost);
		
				foreach (Avatar avt in avatars) {
						if (avt.isSick) {
								Debug.Log (string.Format ("{0} suffered -{1} health from sickness", avt.title, GameParameters.SICK_DAMAGE_PER_DAY));
								avt.AddHealth (-GameParameters.SICK_DAMAGE_PER_DAY);
						}
			
						if (avt.isMissing) {
								int missingDamage = Random.Range (missingDangerInfo.minDamage, missingDangerInfo.maxDamage + 1);
								Debug.Log (string.Format ("{0} suffered -{1} health from missing", avt.title, missingDamage));
								//avt.isMissing = false;
								avt.AddHealth (-missingDamage);
						}
			
			
				}
		}

		void OnAvatarSelect (Avatar avatar)
		{
				activeAvatar = avatar;
		}

		void OnCureStart (Avatar targetAvatar)
		{
				HealByMedic (targetAvatar);
		}
	
		void OnAvatarCureSelect (Avatar avatar)
		{
				activeAvatar = avatar;
		}

		void OnHuntingPlaceSelect (HuntingPlace huntPlace)
		{
				activeHuntingPlace = huntPlace;
				totalEnergyCost = Mathf.Round (huntPlace.energyCost * (1 + currentWeather.obstacle) *
						((activeAvatar.isSick) ? GameParameters.ENERGY_CONSUME_ADD_WHILE_SICK : 1f));
				Debug.Log ("total energy cost: " + totalEnergyCost + " stamina: " + activeAvatar.stamina);
		}

		void OnScavengeCommand (Avatar avatar)
		{
				//activeAvatar = avatar;
		}
	
		void OnScavengeStart (HuntingPlace huntPlace)
		{
				if (activeAvatar.stamina >= totalEnergyCost) {

						LogText.Instance.Log ("============ Scavenge Start  ============================");

						scavangeResult.resourceType = huntPlace.resourceType;
						scavangeResult.gained = 0;
						scavangeResult.lost = 0;
						scavangeResult.damage = 0;
						scavangeResult.dangerType = DangerType.None;
						scavangeResult.justUnlockedNextArea = false;
		
						int itemGain = 0;
						if (huntPlace.resourceType == ResourceType.FOOD) {
								itemGain = FoodGain (huntPlace.probMin, huntPlace.probMax, activeAvatar.attributes.food);
								food.AddValue (itemGain);
								Debug.Log ("Obtained " + itemGain.ToString () + " food!");
						} else if (huntPlace.resourceType == ResourceType.WOOD) {
								itemGain = WoodGain (huntPlace.probMin, huntPlace.probMax, activeAvatar.attributes.wood);
								wood.AddValue (itemGain);
								Debug.Log ("Obtained " + itemGain.ToString () + " wood!");
						}
						scavangeResult.gained = itemGain;
			
						//base_danger_event_chance * (1 + 3*weather) * survive
						float dangerChance = huntPlace.dangerBasePoint * 
								(0.8f + 2 * currentWeather.obstacle) * activeAvatar.attributes.survive;

						LogText.Instance.Log (string.Format ("Rolled dangerChance : {0} from basePoint : {1}, weather {2}, survivability {3}", dangerChance, huntPlace.dangerBasePoint, currentWeather.obstacle, activeAvatar.attributes.survive));

						bool endangered = (Random.Range (0, 1f) < dangerChance);
						if (endangered) {
								DangerEvent dEvent = dangerList.PickDanger ();
								scavangeResult.dangerType = dEvent.type;

								LogText.Instance.Log (string.Format ("Danger SUCCESS : type {0} occurred.", dEvent.type));

								if (dEvent.type == DangerType.PoisonMushroom || dEvent.type == DangerType.PoisonSnake) {
										activeAvatar.isSick = true;
										this.OnStatsChanged ();
								}
			  
								if (dEvent.type == DangerType.Missing) {
										Debug.Log (activeAvatar.title + " is missing!");
										activeAvatar.isMissing = true;
										activeAvatar.gameObject.SetActive (false);
								}
			  
								int dangerDamage = Random.Range (dEvent.minDamage, dEvent.maxDamage + 1);
								if (dangerDamage > 0) {
										activeAvatar.AddHealth (-dangerDamage);
										scavangeResult.damage = dangerDamage;

										LogText.Instance.Log (string.Format ("rolled danger damage : {0} from {1} to {2}", dangerDamage, dEvent.minDamage, dEvent.maxDamage));
								} 
			  
								float dangerItemLossPct = Random.Range (dEvent.minResourceLoss, dEvent.maxResourceLoss);
								int itemLost = 0;
								if (dangerItemLossPct > 0) {
										if (huntPlace.resourceType == ResourceType.FOOD) {
												itemLost = Mathf.RoundToInt ((float)itemGain * dangerItemLossPct);
												food.AddValue (-itemLost);
												Debug.Log ("Lost " + itemLost.ToString () + " food!");
										} else if (huntPlace.resourceType == ResourceType.WOOD) {
												itemLost = Mathf.RoundToInt ((float)itemGain * dangerItemLossPct);
												wood.AddValue (-itemLost);
												Debug.Log ("Lost " + itemLost.ToString () + " wood!");
										}
										scavangeResult.lost = itemLost;

										LogText.Instance.Log (string.Format ("Rolled resource lost percentage : {0} from {1} to {2}", dangerItemLossPct, dEvent.minResourceLoss, dEvent.maxResourceLoss)); 
								}
						} else {
								LogText.Instance.Log (string.Format ("Danger FAILED. Nothing happens."));
						}
			
						int currentHuntPlaceIdx = huntingPlaces.IndexOf (huntPlace);
						if (currentHuntPlaceIdx < 0)
								Debug.Log ("cannot found hunting place in list!");
						else {
								if (currentHuntPlaceIdx < huntingPlaces.Count - 1) {
										HuntingPlace nextHuntPlace = huntingPlaces [currentHuntPlaceIdx + 1];
										if (!nextHuntPlace.isUnlocked) {
												float exploration = 1 * activeAvatar.attributes.explorability * (1 - GameParameters.EXPLORE_WEATHER_MOD * currentWeather.obstacle);
												nextHuntPlace.exploreRequirement -= exploration;

												LogText.Instance.Log (string.Format ("Rolled exploration value : {0} from explorability {1}, weather obstacle {2}", exploration, activeAvatar.attributes.explorability, currentWeather.obstacle));

												if (nextHuntPlace.exploreRequirement <= 0) {
														nextHuntPlace.gameObject.SetActive (true);
														nextHuntPlace.isUnlocked = true;
														scavangeResult.justUnlockedNextArea = true;
												}
										}
								}
						}
			
	
						activeAvatar.AddStamina (-totalEnergyCost);
			
						// announce that scavenge has finished and stats has changed
						this.OnStatsChanged ();
						EventsManager.OnScavengeFinish (scavangeResult);
				} else {
						EventsManager.OnScavengeNotEnoughEnergy ();
						Debug.Log ("not enough energy");
				}
		}

		void OnDoNothing ()
		{
				AddDayPhase ();
		}

		public void AddDayPhase ()
		{
				GameManager.Instance.phaseDay++;
				if (GameManager.Instance.phaseDay >= GameParameters.PHASE_PER_DAY) {
						GameManager.Instance.day++;
						GameManager.Instance.phaseDay = 0;

						EventsManager.OnDayChanged ();
				} else {
						EventsManager.OnDayPhaseChanged ();
				}

				this.OnStatsChanged ();
		}

		int FoodGain (int probMin, int probMax, float foodBonus)
		{
				LogText.Instance.Log (string.Format ("Gained food from prob {0} to {1}, with foodBonus stat {2}", probMin, probMax, foodBonus));
				return Mathf.RoundToInt ((float)(Random.Range (probMin, probMax + 1)) * foodBonus);
		}

		int WoodGain (int probMin, int probMax, float woodBonus)
		{
				LogText.Instance.Log (string.Format ("Gained wood from prob {0} to {1}, with woodBonus stat {2}", probMin, probMax, woodBonus));
				return Mathf.RoundToInt ((float)(Random.Range (probMin, probMax + 1)) * woodBonus);
		}
	
		public void HealByMedic (Avatar avatar)
		{
				if (!avatar.isAlive)
						return;
	  
				Avatar medic = MedicAvatar;
				if (medic.stamina < GameParameters.MEDIC_CURE_COST) 
						EventsManager.OnCureNotEnoughEnergy ();
				else {
						avatar.isSick = false;
						avatar.AddHealth (GameParameters.HEAL_AMOUNT_BY_MEDIC);
	  
						medic.AddStamina (-GameParameters.MEDIC_CURE_COST);
						EventsManager.OnCureFinish (avatar);
				}
		}
	
		int MissingChance (Avatar avatar)
		{
				if (!avatar.isAlive)
						return -1;
				if (day <= 0)
						return -1;
				if (avatar.isMissing || APersonIsMissing)
						return - 1;
	
				float missChance = GameParameters.MISS_BASE_CHANCE * 
						(1 + GameParameters.MISS_CHANCE_WEATHER_MOD * currentWeather.obstacle) *
						avatar.attributes.resistance;
				Debug.Log (string.Format ("{0} miss chance: {1}", avatar.title, missChance));	
				LogText.Instance.Log (string.Format ("{0} : Rolled missing chance = {1} from weather obstacle {2}, avatar resistance {3}", avatar.title, missChance, currentWeather.obstacle, avatar.attributes.resistance)); 
				avatar.isMissing = Random.Range (0, 1f) < missChance;
				if (avatar.isMissing) {
						Debug.Log (avatar.title + " went missing!");
						avatar.gameObject.SetActive (false);
						this.OnStatsChanged ();
						return (int)avatar.role;
				} else
						return -1;

		}
	
		int SickChance (Avatar avatar)
		{
				int sickId = -1;
				if (!avatar.isAlive)
						return sickId;
				if (day <= 0)
						return sickId;
				if (avatar.isSick)
						return sickId;
		
				float sickChance = GameParameters.SICK_BASE_CHANCE * 
						(1 + GameParameters.SICK_CHANCE_WEATHER_MOD * currentWeather.obstacle) *
						avatar.attributes.resistance;	
				Debug.Log (string.Format ("{0} sick chance: {1}", avatar.title, sickChance));
				LogText.Instance.Log (string.Format ("{0} : Rolled sick chance = {1} from weather obstacle {2}, avatar resistance {3}", avatar.title, sickChance, currentWeather.obstacle, avatar.attributes.resistance));
				avatar.isSick = Random.Range (0, 1f) < sickChance;
				if (avatar.isSick) {
						Debug.Log (avatar.title + " fell sick!");
						this.OnStatsChanged ();
						return (int)avatar.role;
				} else
						return -1;
		}

		void RegisterStates ()
		{
				this.AddState (s_None);
				this.AddState (s_BeginDay);
				this.AddState (s_DayPhase);
				this.AddState (s_AvatarSelect);
				this.AddState (s_CurePhase);
				this.AddState (s_CureAvatarSelect);
				this.AddState (s_CureResult);
				this.AddState (s_AreaSelect);
				this.AddState (s_HuntingResult);
				this.AddState (s_EndPhase);
				this.AddState (s_EndDay);
				this.AddState (s_GameOver);
				this.AddState (s_Ending);
				this.AddState (s_ScriptedDialog);

				s_None.parent = this;
				s_BeginDay.parent = this;
				s_DayPhase.parent = this;
				s_AvatarSelect.parent = this;
				s_CurePhase.parent = this;
				s_CureAvatarSelect.parent = this;
				s_CureResult.parent = this;
				s_AreaSelect.parent = this;
				s_HuntingResult.parent = this;
				s_EndPhase.parent = this;
				s_EndDay.parent = this;
				s_Ending.parent = this;
				s_GameOver.parent = this;
				s_ScriptedDialog.parent = this;

				this.GoToState (s_None);
		}
}
