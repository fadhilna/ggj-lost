using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerDisplay : MonoBehaviour
{
	public static GameManagerDisplay Instance;

	public GameManager dataItem;

	// ================= landing area
	public GameObject landingArea;
	public Image[] caveBackgrounds;
	public Image activeBackground;
	public Text foodText;
	public Text woodText;
	public DayItemDisplay[] dayItems;
	public Button scavengeButton;
	public Button doNothingButton;
	public Button medicScavengeButton;
	public Button healButton;
	public Button healOkButton;
	public Button healCancelButton;
	public Button outsideButton;

	// ================= map area
	public Animator mapArea;

	public Animator areaDialogBox;
	public Text areaDialogBoxText;
	public Text areaEnergyCostText;
	public Button areaCancelButton;
	public Button goScavengeButton;

	// ================ result area
	public Animator resultArea;
	public string cutsceneFolder = "Cutscenes/";
	public Image resultImage;
	public Image[] normalResultImage;
	public Image[] endingResultImage;
	public Image[] randomEncounterResultImage;
	public Image[] cureResultImage;
	public Text resultStoryText;
	public Image[] resultIcons;
	public Text resultValueText;

	// ================ end day area
	public Animator endDayArea;

	public Text endDayText;
	public Text endFoodText;
	public Text endWoodText;

	void Awake()
	{
		Instance = this;
		dataItem = this.GetComponent<GameManager> ();
	}
	
	void Start()
	{
		OnDayChanged ();
	}

	void OnEnable()
	{
		dataItem.onStatsChangedE += OnStatsChanged;

		EventsManager.onStateEnterE += OnStateEnter;
		EventsManager.onStateExitE += OnStateExit;
		EventsManager.onDayPhaseChangedE += OnDayPhaseChanged;
		EventsManager.onDayChangedE += OnDayChanged;
		EventsManager.onAvatarSelectE += OnAvatarSelect;
		EventsManager.onAvatarDeselectE += OnAvatarDeselect;
		EventsManager.onAvatarCureSelectE += OnAvatarCureSelect;
		EventsManager.onAvatarCureDeselectE += OnAvatarCureDeselect;
		EventsManager.onCureNotEnoughEnergyE += OnCureNotEnoughEnergy;
		EventsManager.onCureStartE += OnCureStart;
		EventsManager.onHuntingPlaceSelectE += OnHuntingPlaceSelect;
		EventsManager.onScavengeNotEnoughEnergyE += OnNotEnoughEnergy;
		EventsManager.onScavengeFinishedE += OnScavengeFinish;
		EventsManager.onDayBeginFinishE += OnDayBeginFinish;
	}

	void OnDayBeginFinish (BeginDayResult result)
	{
		if (result.missingId >= 0)
		{
			UpdateCutsceneWindow
				(
					Resources.Load<Sprite>(cutsceneFolder + "cuts_missing"),
				 GameManager.Instance.avatars[result.missingId].title + " went missing...",
				 3,
				 "");
		}
		else if (result.sickId.Count > 0)
		{
			UpdateCutsceneWindow
				(
					Resources.Load<Sprite>(cutsceneFolder + "cuts_sick"),
					"Our companion gets a disease.",
					3,
					"");
		}
	}

	void OnDisable()
	{
		dataItem.onStatsChangedE -= OnStatsChanged;

		EventsManager.onStateEnterE -= OnStateEnter;
		EventsManager.onStateExitE -= OnStateExit;
		EventsManager.onDayPhaseChangedE -= OnDayPhaseChanged;
		EventsManager.onDayChangedE -= OnDayChanged;
		EventsManager.onAvatarSelectE -= OnAvatarSelect;
		EventsManager.onAvatarDeselectE -= OnAvatarDeselect;
		EventsManager.onAvatarCureSelectE -= OnAvatarCureSelect;
		EventsManager.onAvatarCureDeselectE -= OnAvatarCureDeselect;
		EventsManager.onCureNotEnoughEnergyE -= OnCureNotEnoughEnergy;
		EventsManager.onCureStartE -= OnCureStart;
		EventsManager.onHuntingPlaceSelectE -= OnHuntingPlaceSelect;
		EventsManager.onScavengeNotEnoughEnergyE -= OnNotEnoughEnergy;
		EventsManager.onScavengeFinishedE -= OnScavengeFinish;
		EventsManager.onDayBeginFinishE -= OnDayBeginFinish;
	}

	#region game state change handler
	
	void OnStateEnter (GameStateNames state)
	{
		switch (state)
		{
		case GameStateNames.None:
			mapArea.SetBool("FadeIn", false);
			break;

		case GameStateNames.BeginDay:
			ChangeBackgroundByPhase (dataItem.phaseDay, (int)dataItem.currentWeather.type);
			break;

		case GameStateNames.DayPhase:
			doNothingButton.gameObject.SetActive(true);
			medicScavengeButton.gameObject.SetActive(false);
			healButton.gameObject.SetActive(false);
			scavengeButton.gameObject.SetActive(false);
			healCancelButton.gameObject.SetActive(false);
			healOkButton.gameObject.SetActive(false);
			outsideButton.gameObject.SetActive(true);

			AudioController.PlayMusic("Music" + ((int)dataItem.currentWeather.type).ToString());
			break;

		case GameStateNames.AvatarSelect:
			break;

		case GameStateNames.CurePhase:
			outsideButton.gameObject.SetActive (false);
			doNothingButton.gameObject.SetActive(false);
			medicScavengeButton.gameObject.SetActive(false);
			healCancelButton.gameObject.SetActive(true);
			break;

		case GameStateNames.CureAvatarSelect:
			break;

		case GameStateNames.CureResult:
			resultArea.SetBool("FadeIn", true);
			break;

		case GameStateNames.AreaSelect:
			mapArea.SetBool("FadeIn", true);
			goScavengeButton.gameObject.SetActive(false);
			break;

		case GameStateNames.HuntingResult:
			break;

		case GameStateNames.EndDay:
			endDayArea.SetBool("FadeIn", true);
			break;

		case GameStateNames.Ending:

			Sprite image = Resources.Load<Sprite>(cutsceneFolder + "cuts_ending");
			UpdateCutsceneWindow(image,	"You survived. Thanks for playing!", 3,	"");

			break;

		case GameStateNames.GameOver:

			Sprite imaji = Resources.Load<Sprite>(cutsceneFolder + "cuts_gameover");
			UpdateCutsceneWindow( imaji, "God has forsaken us once more...", 3,	"");

			break;
		}
	}
	
	void OnStateExit (GameStateNames state)
	{
		switch (state)
		{

		case GameStateNames.AvatarSelect:
			break;

		case GameStateNames.AreaSelect:
			areaDialogBox.SetTrigger ("FadeOut");
			mapArea.SetBool("FadeIn", false);
			break;

		case GameStateNames.CurePhase:
			healButton.gameObject.SetActive(false);
			break;

		case GameStateNames.CureAvatarSelect:
			healOkButton.gameObject.SetActive(false);
			healCancelButton.gameObject.SetActive(false);
			break;

		case GameStateNames.CureResult:
			resultArea.SetBool("FadeIn", false);
			break;
			
		case GameStateNames.HuntingResult:
			mapArea.SetBool("FadeIn", false);
			resultArea.SetBool("FadeIn", false);
			break;

		case GameStateNames.EndDay:
			endDayArea.SetBool("FadeIn", false);
			break;
		}
	}

	#endregion

	#region event handlers

	void OnCureNotEnoughEnergy ()
	{
		StartCoroutine(dataItem.avatars [0].GetComponent<AvatarDisplay> ().PopupDialog ("I am too tired to cure anybody right now."));
	}
	
	void OnDayPhaseChanged ()
	{
		ChangeBackgroundByPhase (dataItem.phaseDay, (int)dataItem.currentWeather.type);
	}
	
	void OnDayChanged ()
	{
		endDayText.text = "END OF DAY " + (dataItem.day).ToString();
		endFoodText.text = string.Format ("{0}  <color=red>(-{1})</color>", dataItem.food.value, dataItem.TotalFoodCost);
		endWoodText.text = string.Format ("{0}  <color=red>(-{1})</color>", dataItem.wood.value, dataItem.TotalWoodCost);
		SetDaysDisplay (dataItem.day);
	}

	void OnAvatarSelect (Avatar avatar)
	{
		doNothingButton.gameObject.SetActive (false);
		medicScavengeButton.gameObject.SetActive (false);
		healButton.gameObject.SetActive (false);
		scavengeButton.gameObject.SetActive(false);

		if (avatar.role == RoleNames.MEDIC)
		{
			medicScavengeButton.gameObject.SetActive(true);
			healButton.gameObject.SetActive(true);
		}
		else
		{
			scavengeButton.gameObject.SetActive(true);
		}
	}

	void OnAvatarDeselect ()
	{
		medicScavengeButton.gameObject.SetActive (false);
		healButton.gameObject.SetActive (false);
		scavengeButton.gameObject.SetActive(false);
		doNothingButton.gameObject.SetActive (true);
	}
	
	void OnAvatarCureSelect (Avatar avatar)
	{
		healOkButton.gameObject.SetActive(true);
	}
	
	void OnAvatarCureDeselect ()
	{
		healOkButton.gameObject.SetActive (false);
	}
	
	void OnStatsChanged ()
	{
		foodText.text = string.Format ("{0}  <color=red>(-{1})</color>", dataItem.food.value, dataItem.TotalFoodCost);
		woodText.text = string.Format ("{0}  <color=red>(-{1})</color>", dataItem.wood.value, dataItem.TotalWoodCost);
	}
	
	void OnCureStart (Avatar targetAvatar)
	{
		Sprite image = Resources.Load<Sprite>(cutsceneFolder + "cuts_cured");

		UpdateCutsceneWindow(
			image,
			targetAvatar.title + " has been cured.",
			2,
			string.Format("{0}", targetAvatar.health)
			);

		AudioController.Play ("Positive3");
	}
	
	void OnHuntingPlaceSelect (HuntingPlace huntPlace)
	{
		UpdateAreaDialogBox (CharacterMapInfoComments.Dialogs[dataItem.activeHuntingPlace.id][dataItem.day / 7], huntPlace);

		goScavengeButton.gameObject.SetActive (true);
	}
	
	void OnNotEnoughEnergy ()
	{
		UpdateAreaDialogBox ("I am not fit enough to go there.", dataItem.activeHuntingPlace);
	}

	void UpdateAreaDialogBox(string dialog, HuntingPlace huntPlace)
	{
		areaDialogBox.SetTrigger ("FadeIn");
		areaDialogBoxText.text = dialog;
		areaEnergyCostText.text = string.Format("{0}  <color=red>(-{1})</color>", dataItem.activeAvatar.stamina, dataItem.totalEnergyCost);
		
		areaDialogBox.GetComponent<AreaDialogBox> ().ChangePortrait ((int)dataItem.activeAvatar.role);
	}
	
	void OnScavengeFinish (ScavangeResult result)
	{
		Sprite image;

		if (result.dangerType == DangerType.None)
		{
			image = Resources.Load<Sprite>(cutsceneFolder + "cuts_normal" + dataItem.activeHuntingPlace.id.ToString());

			AudioController.Play ("Positive3");
		}
		else
		{
			image = Resources.Load<Sprite>(cutsceneFolder + "cuts_encounter" + ((int)result.dangerType - 1).ToString());
			AudioController.Play ("Negative2");
		}

		string resultGained = result.gained > 0 ? "+" +  result.gained.ToString() : "";
		string resultLost = result.lost > 0 ? "(-" + result.lost.ToString() + ")" : "";

		UpdateCutsceneWindow(
			image,
			"This is what I managed to obtain..",
			(int)result.resourceType, 
			string.Format ("<color=green>{0}</color>  <color=red>{1}</color>", resultGained, resultLost)
			);


	}
	#endregion

	#region methods
	
	public void ChangeBackgroundByPhase(int phase, int weatherCond = 0)
	{
		for (int i = 0; i < caveBackgrounds.Length; i++)
		{
			caveBackgrounds[i].gameObject.SetActive(false);
		}
		
		caveBackgrounds [phase * 4 + weatherCond].gameObject.SetActive (true);
		caveBackgrounds [phase * 4 + weatherCond].CrossFadeAlpha (0, 0, false);
		caveBackgrounds [phase * 4 + weatherCond].CrossFadeAlpha (1, 1, true);
	}
	
	void UpdateCutsceneWindow(Sprite image, string dialog, int resultId, string resultNumber)
	{
		resultArea.SetBool("FadeIn", true);
		
		resultIcons [0].gameObject.SetActive (false);
		resultIcons [1].gameObject.SetActive (false);
		resultIcons [2].gameObject.SetActive (false);
		
		resultImage.sprite = image;
		
		resultStoryText.text = dialog;
		
		resultIcons [resultId].gameObject.SetActive (true);
		resultValueText.text = resultNumber;
	}
	
	void SetDaysDisplay(int value)
	{
		// get the multiplier of day first and divide it by 5
		int dayMult = value / 5;
		
		// set the day of that particular mult by the mod
		dayItems [dayMult].ChangeDay ((value % 5));
		
		// for all the value before this, set it to 5
		if (dayMult > 0)
		{
			for (int i = 0; i < dayMult; i++)
			{
				dayItems[i].ChangeDay(4);
			}
		}
		
		// for all value after the mult, set it to nothing
		for (int i = dayMult + 1; i < 5; i++)
		{
			dayItems[i].ChangeDay(-1);
		}
	}

	#endregion

	#region button listener

	public void OnOutsideAreaClicked()
	{
		EventsManager.OnAvatarDeselect ();
	}

	public void OnScavengeClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnScavengeCommand (dataItem.activeAvatar);
	}

	public void OnCureClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnCureCommand ();
	}

	public void OnCureOkClicked()
	{
		EventsManager.OnCureStart (dataItem.activeAvatar);
	}

	public void OnCureCancelClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnCureCancel ();
	}

	public void OnDoNothingClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnDoNothing ();
	}

	public void OnAreaCancelClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnHuntingPlaceCancel ();
	}

	public void OnGoScavengeClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnScavengeStart (dataItem.activeHuntingPlace);
	}

	public void OnOKResultClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnResultComplete ();
	}

	public void OnNextDayClicked()
	{
		AudioController.Play ("Click");
		EventsManager.OnDayChangedFinish ();
	}
	#endregion
}