using UnityEngine;
using System.Collections;

public class GameParameters : MonoBehaviour
{
	public static GameParameters Instance;

	public float basicFoodExcess = 20;
	public float basicWoodExcess = 8;

	public static int PHASE_PER_DAY = 3;
	public static int FOOD_SHORTAGE_DMG_MULTI = 2;
	public static int WOOD_SHORTAGE_DMG_MULTI = 3;
	public static int DANGER_EVENT_WEATHER_MODIFIER = 3;
	public static float ENERGY_CONSUME_ADD_WHILE_SICK = 1.3f;
	public static int SICK_DAMAGE_PER_DAY = 20;
	public static int ENERGY_REGEN_PER_DAY = 10;
	public static int HEAL_AMOUNT_BY_MEDIC = 25;
	public static float MISS_BASE_CHANCE = 0.03f;
	public static int MISS_CHANCE_WEATHER_MOD = 3;
	public static float SICK_BASE_CHANCE = 0.05f;
	public static int SICK_CHANCE_WEATHER_MOD = 4;
	public static int EXPLORE_WEATHER_MOD = 2;
	public static int MEDIC_CURE_COST = 10;

	void Awake()
	{
		Instance = this;
	}
}