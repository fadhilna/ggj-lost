public enum GameStateNames
{
	None,
	BeginDay,
	DayPhase,
	AvatarSelect,
	CurePhase,
	CureAvatarSelect,
	CureResult,
	AreaSelect,
	HuntingResult,
	EndPhase,
	EndDay,
	GameOver,
	Ending
}