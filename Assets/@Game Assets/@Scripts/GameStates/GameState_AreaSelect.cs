using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_AreaSelect : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.AreaSelect);

		EventsManager.onHuntingPlaceCancelE += OnHuntCancel;
		EventsManager.onScavengeFinishedE += OnScavengeFinish;
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.AreaSelect);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onHuntingPlaceCancelE -= OnHuntCancel;
		EventsManager.onScavengeFinishedE -= OnScavengeFinish;
	}
	
	void OnHuntCancel ()
	{
		parent.GoToState (parent.s_DayPhase);
	}
	
	void OnScavengeFinish (ScavangeResult result)
	{
		parent.GoToState (parent.s_HuntingResult, result);
	}
}