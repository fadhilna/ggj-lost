using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_AvatarSelect : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.onScavengeCommandE += OnScavengeCommand;
		EventsManager.onCureCommandE += OnCureCommand;
		EventsManager.onAvatarDeselectE += OnAvatarDeselect;
		EventsManager.OnStateEnter (GameStateNames.AvatarSelect);
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.AvatarSelect);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onScavengeCommandE -= OnScavengeCommand;
		EventsManager.onCureCommandE -= OnCureCommand;
		EventsManager.onAvatarDeselectE -= OnAvatarDeselect;
	}
	
	void OnScavengeCommand (Avatar avatar)
	{
		parent.GoToState (parent.s_AreaSelect);
	}
	
	void OnCureCommand ()
	{
		parent.GoToState (parent.s_CurePhase);
	}
	
	void OnAvatarDeselect ()
	{
		parent.GoToState (parent.s_DayPhase);
	}
}