using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_BeginDay : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.BeginDay);

		parent.GoToState (parent.s_DayPhase);
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.BeginDay);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));
	}
}