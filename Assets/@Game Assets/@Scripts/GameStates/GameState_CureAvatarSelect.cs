using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_CureAvatarSelect : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.CureAvatarSelect);

		EventsManager.onCureFinishE += OnCureFinish;
		EventsManager.onCureNotEnoughEnergyE += OnCureNotEnoughEnergy;
		EventsManager.onCureCancelE += OnCureCancel;
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.CureAvatarSelect);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onCureStartE -= OnCureFinish;
		EventsManager.onCureNotEnoughEnergyE -= OnCureNotEnoughEnergy;
		EventsManager.onCureCancelE -= OnCureCancel;
	}
	
	void OnCureFinish (Avatar targetAvatar)
	{
		parent.GoToState (parent.s_CureResult);
	}
	
	void OnCureCancel ()
	{
		parent.GoToState (parent.s_DayPhase);
	}
	
	void OnCureNotEnoughEnergy ()
	{
		parent.GoToState (parent.s_DayPhase);
	}
}