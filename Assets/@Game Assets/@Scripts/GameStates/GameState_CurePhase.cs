using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_CurePhase : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.CurePhase);

		EventsManager.onAvatarCureSelectE += OnCureAvatarSelect;
		EventsManager.onCureCancelE += OnCureCancel;
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.CurePhase);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onAvatarCureSelectE -= OnCureAvatarSelect;
		EventsManager.onCureCancelE -= OnCureCancel;
	}
	
	
	void OnCureAvatarSelect (Avatar targetAvatar)
	{
		parent.GoToState (parent.s_CureAvatarSelect);
	}
	
	void OnCureCancel ()
	{
		parent.GoToState (parent.s_DayPhase);
	}
}