using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_CureResult : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.CureResult);

		EventsManager.onResultCompleteE += OnResultComplete;
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.CureResult);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onResultCompleteE -= OnResultComplete;
	}

	void OnResultComplete ()
	{
		parent.GoToState (parent.s_EndPhase);
	}
}