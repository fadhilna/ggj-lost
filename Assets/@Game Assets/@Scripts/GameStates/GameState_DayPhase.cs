using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_DayPhase : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.onAvatarSelectE += OnAvatarSelect;

		EventsManager.onDayChangedE += OnDayChanged;

		EventsManager.onResultCompleteE += OnResultComplete;

		EventsManager.OnStateEnter (GameStateNames.DayPhase);

		if (GameManager.Instance.isGameOver)
			parent.GoToState(parent.s_GameOver);
		else if (GameManager.Instance.isGameWin)
			parent.GoToState(parent.s_Ending);
	}

	void OnResultComplete ()
	{
		
		GameManagerDisplay.Instance.resultArea.SetBool("FadeIn", false);
	}

	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.onAvatarSelectE -= OnAvatarSelect;

		EventsManager.onDayChangedE -= OnDayChanged;

		EventsManager.onResultCompleteE -= OnResultComplete;

		EventsManager.OnStateExit (GameStateNames.DayPhase);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));
	}

	void OnAvatarSelect (Avatar avatar)
	{
		parent.GoToState (parent.s_AvatarSelect);
	}
	
	void OnDayChanged ()
	{
		parent.GoToState (parent.s_EndDay);
	}

}