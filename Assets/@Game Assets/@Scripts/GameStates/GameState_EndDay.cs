using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_EndDay : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.EndDay);

		EventsManager.onDayChangeFinishE += OnDayChangeFinish;

		AudioController.PlayMusic ("Night");
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.EndDay);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onDayChangeFinishE -= OnDayChangeFinish;
	}
	
	void OnDayChangeFinish ()
	{
		parent.GoToState (parent.s_BeginDay);
	}
}