using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_EndPhase : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.onDayChangedE += OnDayChanged;
		EventsManager.onDayPhaseChangedE += OnDayPhaseChanged;

		EventsManager.OnStateEnter (GameStateNames.EndPhase);
	}
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.EndPhase);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onDayChangedE -= OnDayChanged;
		EventsManager.onDayPhaseChangedE -= OnDayPhaseChanged;
	}
	

	void OnDayChanged ()
	{
		parent.GoToState (parent.s_EndDay);
	}
	
	void OnDayPhaseChanged ()
	{
		parent.GoToState (parent.s_DayPhase);
	}

}