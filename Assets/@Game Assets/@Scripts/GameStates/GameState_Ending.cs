using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_Ending : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.Ending);

		AudioController.StopMusic ();
		AudioController.Play ("Ending");

		EventsManager.onResultCompleteE += OnResultComplete;
	}
	
	public override void OnUpdate()
	{
		EventsManager.onResultCompleteE -= OnResultComplete;
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.Ending);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));
	}

	void OnResultComplete ()
	{
		
		EventsManager.onResultCompleteE -= OnResultComplete;
		Application.LoadLevel ("GameTitle");
	}
}