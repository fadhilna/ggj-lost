using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_GameOver : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.GameOver);

		AudioController.StopMusic ();
		AudioController.Play ("GameOver");

		EventsManager.onResultCompleteE += OnResultComplete;
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.GameOver);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onResultCompleteE -= OnResultComplete;
	}
	
	void OnResultComplete ()
	{
		
		EventsManager.onResultCompleteE -= OnResultComplete;
		Application.LoadLevel ("GameTitle");
	}
}