using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_HuntingResult : FSMState
{
	public GameManager parent;
	
	public override void OnEnter(object param)
	{
		ScavangeResult result = (ScavangeResult)param;
		EventsManager.OnStateEnter (GameStateNames.HuntingResult);

		EventsManager.onResultCompleteE += OnScavengeResultComplete;
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.HuntingResult);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));

		EventsManager.onResultCompleteE -= OnScavengeResultComplete;
	}
	
	void OnScavengeResultComplete ()
	{
		parent.GoToState (parent.s_EndPhase);
	}
}