using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameState_None : FSMState
{
	public GameManager parent;
	
	public override void OnEnter()
	{
		EventsManager.OnStateEnter (GameStateNames.None);

		parent.GoToState (parent.s_BeginDay);
	}
	
	public override void OnUpdate()
	{
	}
	
	public override void OnExit()
	{
		EventsManager.OnStateExit (GameStateNames.None);
		Debug.Log (string.Format ("Exit : {0} , Enter {1}", parent.CurrentState, parent.NextState));
	}
}