using UnityEngine;
using System.Collections;

public class GameUIManager : MonoBehaviour
{
	public static GameUIManager Instance;

	void Awake()
	{
		Instance = this;
	}

	void OnEnable()
	{
		EventsManager.onStateEnterE += OnStateEnter;
		EventsManager.onStateExitE += OnStateExit;
	}

	void OnDisable()
	{
		EventsManager.onStateExitE -= OnStateEnter;
		EventsManager.onStateExitE -= OnStateExit;
	}
	
	void OnStateEnter (GameStateNames state)
	{
		switch (state)
		{

		}
	}
	
	void OnStateExit (GameStateNames state)
	{
		
	}
}