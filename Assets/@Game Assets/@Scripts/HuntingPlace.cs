﻿using UnityEngine;
using System.Collections;

public class HuntingPlace : MonoBehaviour
{
	public int id;
	public string title = "Forest";
	public ResourceType resourceType;
	public int probMin;
	public int probMax;
	public float dangerBasePoint;
	public int energyCost;
	public bool isUnlocked;
	public float exploreRequirement;

	// events
	public delegate void StatsChangedHandler();
	public event StatsChangedHandler onStatsChangedE;
	public void OnStatsChanged() { if (onStatsChangedE != null)	onStatsChangedE (); }

	void Start()
	{
		InitStats ();
	}

	void InitStats()
	{
		OnStatsChanged ();
	}
}
