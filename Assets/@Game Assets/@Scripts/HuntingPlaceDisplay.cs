using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HuntingPlaceDisplay : MonoBehaviour
{
	public HuntingPlace dataItem;

	void Awake()
	{
		dataItem = this.GetComponent<HuntingPlace> ();
	}

	void OnEnable()
	{
		EventsManager.onStateEnterE += OnStateEnter;
		EventsManager.onStateExitE += OnStateExit;
	}

	void OnDisable()
	{
		EventsManager.onStateEnterE -= OnStateEnter;
		EventsManager.onStateExitE -= OnStateExit;
	}
	
	void OnStateEnter (GameStateNames state)
	{
		switch (state)
		{
		case GameStateNames.BeginDay:
			break;

		case GameStateNames.AreaSelect:
			break;
		}
	}
	
	void OnStateExit (GameStateNames state)
	{
		switch (state)
		{
		case GameStateNames.AreaSelect:
			break;
		}
	}

	public void OnHuntingPlaceClicked()
	{
		EventsManager.OnHuntingPlaceSelect (this.dataItem);
	}

}
