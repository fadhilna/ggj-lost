﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LogText : MonoBehaviour {

	public static LogText Instance;
	private Text itemText;
	private List<string> logItems;
	private int maxLines = 9 ;

	void Awake()
	{
		Instance = this;
		itemText = this.GetComponent<Text>();

		logItems = new List<string>();
	}

	public void Log(string text)
	{
		logItems.Add (text);
		if (logItems.Count > maxLines)
		{
			logItems.RemoveAt(0);
		}

		string displayText = "";
		for (int i = 0; i < logItems.Count; i++)
		{
			if( i != logItems.Count - 1 ) 
				displayText +=string.Format("<color=yellow>{0}</color>\n", logItems[i]);
			else
				displayText += string.Format("<color=yellow>{0}</color>", logItems[i]);
		}

		itemText.text = displayText;

	}
}
