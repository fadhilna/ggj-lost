﻿using UnityEngine;
using System.Collections;

public class OpeningController : MonoBehaviour {

	public Animator cutSceneAnim;

	public int iter;
	public void OnNextTap()
	{
		cutSceneAnim.SetTrigger ("FadeIn");
		iter++;

		if (iter > 2)
			Application.LoadLevel("GameScene");
	}
}
