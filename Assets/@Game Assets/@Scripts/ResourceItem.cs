﻿using UnityEngine;
using System.Collections;

public class ResourceItem : MonoBehaviour 
{
	public ResourceType type;
	public float maxValue;
	public float value;

	// events
	public delegate void StatsChangedHandler();
	public event StatsChangedHandler onStatsChangedE;
	public void OnStatsChanged() { if (onStatsChangedE != null)	onStatsChangedE (); }

	void Awake()
	{
		InitStats ();
	}

	void Start()
	{
	
	}
	
	void InitStats()
	{
		if(type == ResourceType.FOOD)
		  value = 50;
		else if(type == ResourceType.WOOD)
		  value = 20;
		OnStatsChanged ();
	}

	public void AddValue(float amount)
	{
		value = Mathf.Clamp (value + amount, 0, maxValue);
		OnStatsChanged ();
	}
}
