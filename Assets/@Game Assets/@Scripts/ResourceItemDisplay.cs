using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourceItemDisplay : MonoBehaviour
{
	public ResourceItem dataItem;

	public Text valueText;
	public Slider valueSlider;

	void Awake()
	{
		dataItem = this.GetComponent<ResourceItem> ();
	}

	void OnEnable()
	{
		dataItem.onStatsChangedE += OnStatsChanged;
	}

	void OnStatsChanged ()
	{
		valueText.text = dataItem.type.ToString () + " : " + dataItem.value.ToString() + " / " + dataItem.maxValue.ToString ();
		valueSlider.value = (dataItem.value / dataItem.maxValue);
	}

	void OnDisable()
	{
		dataItem.onStatsChangedE -= OnStatsChanged;
	}
}