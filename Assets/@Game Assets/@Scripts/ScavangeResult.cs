﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScavangeResult
{
	public ResourceType resourceType;
	public int gained;
	public int lost;
	public int damage;
	public DangerType dangerType;
	public bool justUnlockedNextArea;
}
