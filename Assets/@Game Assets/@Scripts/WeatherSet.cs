﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeatherSet : MonoBehaviour {

	public WeatherCondition[] weathers;
	public float[] chances;
	
	float totalChance = 0;
	List<float> chanceSegments;
	
	// Use this for initialization
	void Start () {
		
	}
	
	void Awake() {
		if(weathers.Length != chances.Length)
		  Debug.Log("WARNING: weather types and chances don't match!!");
	
		float currentChangeSegment = 0;
		chanceSegments = new List<float>();
		for(int i=0;i<weathers.Length;i++) {
		  currentChangeSegment += chances[i];
		  totalChance += chances[i];
		  chanceSegments.Add(currentChangeSegment);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public WeatherCondition PickWeather() {
	  float pick = Random.Range(0, totalChance);
	  for(int i=0;i<chanceSegments.Count;i++) {
	    if(pick < chanceSegments[i]) {
	      Debug.Log("picked weather: "+weathers[i].name);
	      return weathers[i];
	    }
	  }
	  return null;
	}
}
