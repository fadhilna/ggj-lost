﻿using UnityEngine;
using System.Collections;

public enum WeatherType {
  Sunny,
  Foggy,
  Rainy,
  Storm
}

public class WeatherSystem : MonoBehaviour {
	
	public WeatherSet[] weatherSets;
	public int[] firstOccurences;
	
	// Use this for initialization
	void Start () {
		if(weatherSets.Length != firstOccurences.Length) {
		  Debug.Log("WARNING! weather sets and and first occurences length don't match");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public WeatherSet PickWeatherSet(int day) {
	  int setIdx = 0;
	  for(int i=0;i<firstOccurences.Length;i++) {
	    if(firstOccurences[i] > day) {
	      Debug.Log(string.Format("weather set {0} for day {1}",i-1,day));
	      break;
	    }
	    else {
	      if(i == firstOccurences.Length-1)
			Debug.Log(string.Format("weather set {0} for day {1}",i,day));
	      setIdx = i;
	    }
	  }
	  
	  return weatherSets[setIdx];
	}
}
