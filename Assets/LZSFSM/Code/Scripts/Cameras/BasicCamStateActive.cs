using UnityEngine;
using System.Collections;

[System.Serializable]
public class BasicCamStateActive : FSMState {
	
	[System.NonSerialized]
	public CameraBasic Parent;
	
	// Only need to use the OnEnter() rather than the OnEnter(object userData)
	// This is because we do not want to pass any information to the IDLE state
	public override void OnEnter()
	{
		Debug.Log(string.Format("{0}: Entering", this));
	}
	
	public override void OnUpdate()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			Debug.Log(Parent.NextState);
			Parent.GoToState(Parent.S_BasicCamOrbit);
		}
			
		if (Input.GetButtonDown("Fire2"))
			Parent.PushState(Parent.S_BasicCamMouseLook);
	}
	
	public override void OnExit()
	{
		Debug.Log(string.Format("{0}: Exiting for {1}", this, Parent.NextState));
	}
}
