using UnityEngine;
using System.Collections;

[System.Serializable]
public class BasicCamStateIdle : FSMState {
	
	[System.NonSerialized]
	public CameraBasic Parent;
	
	// Only need to use the OnEnter() rather than the OnEnter(object userData)
	// This is because we do not want to pass any information to the IDLE state
	public override void OnEnter()
	{		
		Debug.Log(string.Format("{0}: Entering", this));
	}
	
	public override void OnUpdate()
	{
		Parent.GoToState(Parent.S_BasicCamActive);
	}
	
	public override void OnExit()
	{
		Debug.Log(string.Format("{0}: Exiting", this));
	}
}
