using UnityEngine;
using System.Collections;

[System.Serializable]
public class BasicCamStateMouseLook : FSMState {
	
	[System.NonSerialized]
	public CameraBasic Parent;
	
	#region specific MouseLook Members
	
	private float	_sensitivityX 		= 15f;
	private float	_sensitivityY		= 15f;
		
	private float 	_rotationY 			= 0f;
	private float	_rotationX			= 0f;
	
	#endregion
	
	// Only need to use the OnEnter() coming from "Fire2" in BasicCamStateActive.OnUpdate() rather than the OnEnter(object userData)
	// This is because we do not want to pass any information to the IDLE state
	public override void OnEnter()
	{		
		Debug.Log(string.Format("{0}: Entering", this));
	}
	
	public override void OnUpdate()
	{		
		_rotationX = Input.GetAxis("Mouse X") * _sensitivityX;
		_rotationY = Input.GetAxis("Mouse Y") * _sensitivityY;
		
		Parent.transform.localEulerAngles += new Vector3( -_rotationY, _rotationX, 0 );
	}
	
	// This states "Reason" function which basically checks every update before doing anything else
	// to see if we should still infact be in this state.
	public override void Reason()
	{
		// Letting go of the right click will pop the state back into the state from which we were pushed
		if (Input.GetButtonUp("Fire2") && Parent._statePushed)
		{
			Parent.PopState();
		}
	}
	
	public override void OnExit()
	{
		Debug.Log(string.Format("{0}: Exiting", this));
	}
}

