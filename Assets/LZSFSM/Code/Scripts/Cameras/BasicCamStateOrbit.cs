using UnityEngine;
using System.Collections;

[System.Serializable]
public class BasicCamStateOrbit : FSMState {
	
	[System.NonSerialized]
	public CameraBasic Parent;
	
	#region MouseOrbit Specific Members
	public float			Distance		= 80f;
	
	// Does this camera have a look at target
	// if so then what is it. Set in Inspector.
	public Transform		LookAtTarget	= null;

	private float			XSensitivity	= 0.2f;
	private float			YSensitivity	= 4f;
	
	private Vector3			_moveVector		= Vector3.zero;
	private float			_angle			= 0f;
	
	private float			_height			= 0f;
	private float			_minHeight		= -100f;
	private float			_maxHeight		= 100f;
	
	private float			_scrollSpeed	= 50f;
	private float			_minDistance	= 80f;
	private float			_maxDistance	= 300f;
	
	#endregion
	
	// Only need to use the OnEnter() rather than the OnEnter(object userData)
	// This is because we do not want to pass any information to the IDLE state
	public override void OnEnter()
	{
		Debug.Log(string.Format("{0}: Entering ...", this));
		
		Parent.MouseOrbitActive = true;
		
		if (LookAtTarget == null)
		{
			Debug.LogError(string.Format("ERROR {0} : Has no LookAtTarget but is set to LookAt", this));
			Parent.GoToState(Parent.S_BasicCamActive);
		}
		
		Distance = Vector3.Distance(Parent.transform.position, LookAtTarget.transform.position);
	}
	
	// Now what does this state need to do while running ?
	public override void OnUpdate()
	{
		if (Input.GetButtonDown("Fire2"))
			Parent.PushState(Parent.S_BasicCamMouseLook);
		
		// Set the angle were viewing the planet at basedon the MouseX
		if (Input.GetAxis("Mouse X") != 0)
			_angle += Input.GetAxis("Mouse X") * XSensitivity;
			
		// Set the height from the MouseY and then clamp it
		_height -= Input.GetAxis("Mouse Y") * YSensitivity;
		_height = Mathf.Clamp( _height, _minHeight, _maxHeight);
			
		// Set the distance from the MouseScrollWheel and then Clamp it
		Distance += -Input.GetAxis("Mouse ScrollWheel") * _scrollSpeed;
		Distance = Mathf.Clamp(Distance, _minDistance, _maxDistance);
		
		_moveVector.x = LookAtTarget.position.x + (Mathf.Sin(-_angle)* Distance);
		_moveVector.z = LookAtTarget.position.z - (Mathf.Cos(-_angle)* Distance);
		_moveVector.y = _height;
		
		Parent.transform.position = _moveVector;

		Parent.transform.LookAt(LookAtTarget);
	}
	
	// This states "Reason" function which basically checks every update before doing anything else
	// to see if we should still infact be in this state.
	public override void Reason()
	{
		if (Input.GetButtonUp("Fire1"))
		{
			Parent.GoToPreviousState();
		}
	}
	
	// When exiting all we need to do is set the MouseOrbitActive to false for obvious reasons
	public override void OnExit()
	{
		Parent.MouseOrbitActive = false;
		
		Debug.Log(string.Format("{0}: Exiting", this));
	}
}
