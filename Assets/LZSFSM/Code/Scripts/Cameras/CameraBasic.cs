using UnityEngine;
using System.Collections;

/// <summary>
/// Basic main camera script
/// Currently has 2 states :
/// </summary>
public class CameraBasic : FSMSystem {
	
	// Does this camera have MouseOrbitActive ?
	public bool				MouseOrbitActive	= false;
	
	// Does this camera have MouseLookActive ?
	// This is looking left and right and up and down within the constraints 
	public bool				MouseLookActive		= false;
	
	// Declare this FSM's States
	public BasicCamStateIdle 		S_BasicCamIdle;
	public BasicCamStateActive 		S_BasicCamActive;
	public BasicCamStateMouseLook 	S_BasicCamMouseLook;
	public BasicCamStateOrbit		S_BasicCamOrbit;
	
	void Awake() {
		
		// Now add each state to the FSM and dont forget to set its parent
		// We have to add in the parent now because we do not know the Type until runtime
		AddState(S_BasicCamIdle);
		S_BasicCamIdle.Parent 		= this;
		
		AddState(S_BasicCamActive);
		S_BasicCamActive.Parent 	= this;
		
		AddState(S_BasicCamMouseLook);
		S_BasicCamMouseLook.Parent 	= this;
		
		AddState(S_BasicCamOrbit);
		S_BasicCamOrbit.Parent 		= this;
		
	}
}
